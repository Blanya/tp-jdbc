package org.example.domain;

import org.example.dao.ProduitDaoImpl;
import org.example.domain.Produit;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Menu {

    //use method
    static ProduitDaoImpl produitDao = new ProduitDaoImpl();

    //print menu and get choice
    public static void printMenu()
    {
       String[] menu = {"---Liste de Produit---", "1. Enregistrer un produit", "2. Afficher la liste des produits",
       "3. Afficher un produit", "4. Mise un jour d'un produit", "5. Supprimer un produit", "6. Exit"};

        for (String prop : menu) {
            System.out.println(prop);
        }

        Scanner sc = new Scanner(System.in);
        int choice = 0;

        try
        {
            choice = sc.nextInt();
        }catch(InputMismatchException e)
        {
            System.out.println("Erreur: Veuillez entrez un nombre");
            printMenu();
        }

            switch (choice) {
                case 1:
                    System.out.println("Allons créer un produit!");
                    createProduct();
                    printMenu();
                    break;
                case 2:
                    System.out.println("Voici la liste des produits");
                    showAllProducts();
                    printMenu();
                    break;
                case 3:
                    System.out.println("Voici le détail du produit choisi");
                    showOneproduct();
                    printMenu();
                    break;
                case 4:
                    System.out.println("Allons modifier le produit!");
                    updateProduct();
                    printMenu();
                    break;
                case 5:
                    System.out.println("Allons supprimer un produit!");
                    deleteProduct();
                    printMenu();
                    break;
                case 6:
                    System.out.println("Au revoir !");
                    break;
                default:
                    System.out.println("Commande invalide, veuillez réessayer");
                    printMenu();
            }
    }

    //get products list in db
    public static void showAllProducts()
    {
        List<Produit> products = produitDao.getAllProducts();

        for (Produit p: products ) {
            System.out.println(p);
        }
    }

    //get one product with id
    public static void showOneproduct()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Donnez-moi l'id du produit");

        try{
            int id = sc.nextInt();
            Produit p = produitDao.getProduct(id);
            //if id doesn't exist => product == null
            if(p == null)
            {
                System.out.println("Le produit recherché n'existe pas");
            }
            else
            {
                System.out.println(p);
            }
        }catch (InputMismatchException e)
        {
            System.out.println("Erreur: Veuillez entrez un nombre");
            showOneproduct();
        }

    }

    public static void createProduct()
    {
        Scanner sc = new Scanner(System.in);

        try{
            System.out.println("Nom: ");
            String name = sc.nextLine();
            System.out.println("Prix: ");
            double price = sc.nextDouble();
            System.out.println("Quantité: ");
            Integer quantity = sc.nextInt();
            System.out.println("Description: ");
            sc.nextLine();
            String description = sc.nextLine();

            //get values from scanner
            Produit p = new Produit(name,price,quantity,description);

            // result > 0 if executed in db
            Integer result = produitDao.createProduct(p);
            if(result>0){
                System.out.println("Produit créé !");
            }else{
                System.out.println("Erreur dans la création du produit");
            }
        }catch (InputMismatchException ex){
            System.out.println("Données incorrectes, veuillez réessayer.");
            createProduct();
        }
    }

    public static void updateProduct()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Donnez-moi le produit à modifier et les élèments à modifier");
        System.out.println("Id du produit");

        try{
            int id = sc.nextInt();
            Produit p = produitDao.getProduct(id);

            // p == null if id doesn't exist product same
            if(p.equals(null))
            {
                System.out.println("Le produit sélectionné n'existe pas, veuillez entrer un numéro de produit valide");
                updateProduct();
            }
            else
            {
                //get product values and new values
                System.out.printf("Nom[%s] :", p.getName());
                sc.nextLine();
                String name = sc.nextLine();

                System.out.printf("Prix[%f] :", p.getPrice());
                double price = sc.nextDouble();

                System.out.printf("Quantité[%d] :", p.getQuantity());
                int quantity = sc.nextInt();

                System.out.printf("Description[%s] :", p.getDescription());
                sc.nextLine();
                String description = sc.nextLine();

                p.setName(name);
                p.setPrice(price);
                p.setQuantity(quantity);
                p.setDescription(description);

                int result = produitDao.updateProduct(p);

                if(result > 0)
                {
                    System.out.println("Le produit a bien été mis à jour");
                }
                else
                {
                    System.out.println("Erreur lors de la modification du produit");
                }
            }
        }catch (InputMismatchException e)
        {
            System.out.println("Les données insérées sont incorrectes, veuillez réessayer");
            updateProduct();
        }
    }

    public static void deleteProduct()
    {
        System.out.println("Donnez-moi l'id du produit à supprimer");
        Scanner sc = new Scanner(System.in);

        try {
            int id = sc.nextInt();

            //check if product exists
            if(produitDao.getProduct(id) != null)
            {
               int result = produitDao.deleteProduct(id);

                if(result > 0)
                {
                    System.out.println("Produit supprimé ! ");
                }
                else
                {
                    System.out.println("Erreur lors de la suppression");
                }
            }
            //product doesn't exist
            else
            {
                System.out.println("Le produit n'existe pas, veuillez réessayer");
                deleteProduct();
            }

        } catch (InputMismatchException e)
        {
            System.out.println("Veuillez entrer un nombre");
        }
    }
}
