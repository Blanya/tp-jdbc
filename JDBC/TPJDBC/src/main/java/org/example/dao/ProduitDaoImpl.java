package org.example.dao;

import com.mysql.cj.jdbc.exceptions.CommunicationsException;
import org.example.domain.Produit;
import org.example.utils.ConnectionUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProduitDaoImpl implements IDaoProduit {
    @Override
    public Produit getProduct(Integer id) {
        Produit p = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;

        try {
            connection = ConnectionUtils.getMySQLConnection();
            String sql = "SELECT * FROM produit WHERE id_pr = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            rs = statement.executeQuery();

            if (rs.next()){
                p = new Produit(rs.getString("name_pr"),
                        rs.getDouble("price_pr"),
                        rs.getInt("quantity_pr"),
                        rs.getString("description_pr"));

                p.setId(rs.getInt("id_pr"));
            }
        }catch (SQLSyntaxErrorException ex) {
            System.err.println("Error : " + ex.getMessage());
        } catch (CommunicationsException exception)
        {
            System.err.println("Error : Connexion non établie");
        } catch (SQLException exception1)
        {
            exception1.printStackTrace();
        }
        finally {
            try {
                if(rs != null)
                    rs.close();
                if(statement != null)
                    statement.close();
                if(connection != null)
                    connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        return p;
    }

    @Override
    public List<Produit> getAllProducts() {
        List<Produit> products = new ArrayList<>();
        Produit p = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;

        try {
            connection = ConnectionUtils.getMySQLConnection();
            String sql = "SELECT * FROM produit";
            statement = connection.prepareStatement(sql);
            rs = statement.executeQuery();

            while (rs.next()){
                p = new Produit();
                p.setId(rs.getInt("id_pr"));
                p.setName(rs.getString("name_pr"));
                p.setPrice(rs.getDouble("price_pr"));
                p.setQuantity(rs.getInt("quantity_pr"));
                p.setDescription(rs.getString("description_pr"));

                products.add(p);
            }
        }catch (SQLSyntaxErrorException ex) {
            System.err.println("Error : " + ex.getMessage());
        } catch (CommunicationsException exception)
        {
            System.err.println("Error : Connexion non établie");
        } catch (SQLException exception1)
        {
            exception1.printStackTrace();
        }
        finally {
            try {
                if(rs != null)
                    rs.close();
                if(statement != null)
                    statement.close();
                if(connection != null)
                    connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        return products;
    }

    @Override
    public Integer createProduct(Produit produit) {
        Connection connection = null;
        PreparedStatement statement = null;
        int result = 0;

        try {
            connection = ConnectionUtils.getMySQLConnection();
            String sql = "INSERT INTO produit(name_pr, price_pr, quantity_pr, description_pr) VALUES (?, ?, ?, ?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1, produit.getName());
            statement.setDouble(2, produit.getPrice());
            statement.setInt(3, produit.getQuantity());
            statement.setString(4, produit.getDescription());

            result = statement.executeUpdate();

        }catch (SQLSyntaxErrorException ex) {
            System.err.println("Error : " + ex.getMessage());
        } catch (CommunicationsException exception)
        {
            System.err.println("Error : Connexion non établie");
        } catch (SQLException exception1)
        {
            exception1.printStackTrace();
        }
        finally {
            try {
                if(statement != null)
                    statement.close();
                if(connection != null)
                    connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        return result;
    }

    @Override
    public Integer updateProduct(Produit produit) {
        Connection connection = null;
        PreparedStatement statement = null;
        int result = 0;

        try {
            connection = ConnectionUtils.getMySQLConnection();
            String sql = "UPDATE produit SET name_pr = ?, price_pr = ?, quantity_pr = ?, description_pr = ? WHERE id_pr = ?";
            statement = connection.prepareStatement(sql);

            statement.setString(1, produit.getName());
            statement.setDouble(2, produit.getPrice());
            statement.setInt(3, produit.getQuantity());
            statement.setString(4, produit.getDescription());
            statement.setInt(5, produit.getId());

            result = statement.executeUpdate();

        }catch (SQLSyntaxErrorException ex) {
            System.err.println("Error : " + ex.getMessage());
        } catch (CommunicationsException exception)
        {
            System.err.println("Error : Connexion non établie");
        } catch (SQLException exception1)
        {
            exception1.printStackTrace();
        }
        finally {
            try {
                if(statement != null)
                    statement.close();
                if(connection != null)
                    connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        return result;
    }

    @Override
    public Integer deleteProduct(Integer id) {
        Connection connection = null;
        PreparedStatement statement = null;
        int result = 0;

        try {
            connection = ConnectionUtils.getMySQLConnection();
            String sql = "DELETE FROM produit WHERE id_pr = ?";
            statement = connection.prepareStatement(sql);

            statement.setInt(1, id);

            result = statement.executeUpdate();

        }catch (SQLSyntaxErrorException ex) {
            System.err.println("Error : " + ex.getMessage());
        } catch (CommunicationsException exception)
        {
            System.err.println("Error : Connexion non établie");
        } catch (SQLException exception1)
        {
            exception1.printStackTrace();
        }
        finally {
            try {
                if(statement != null)
                    statement.close();
                if(connection != null)
                    connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        return result;
    }
}
