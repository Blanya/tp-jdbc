package org.example.dao;

import org.example.domain.Produit;

import java.util.List;

public interface IDaoProduit {

    Produit getProduct(Integer id);

    List<Produit> getAllProducts();

    Integer createProduct(Produit produit);

    Integer updateProduct(Produit produit);

    Integer deleteProduct(Integer id);

}
