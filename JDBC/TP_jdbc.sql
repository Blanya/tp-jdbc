USE bd_mysql;

CREATE TABLE produit(
	id_pr INT PRIMARY KEY AUTO_INCREMENT,
    name_pr VARCHAR(100),
    price_pr FLOAT(7,2),
    quantity_pr INT,
    description_pr VARCHAR(500)
);

INSERT INTO produit(name_pr, price_pr, quantity_pr, description_pr) VALUES('table bois laqué', 258.25, 5, 'Table blanche, 4 pieds, bois laqué'), 
('table bois laqué', 278.25, 2, 'Table blanche, 3 pieds, bois laqué'), ('chaise en velours', 62.99, 15, 'De couleur verte pomme, 4 pieds');

SELECT * FROM produit;