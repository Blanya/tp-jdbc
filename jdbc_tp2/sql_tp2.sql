CREATE DATABASE bdd_voiture;

USE bdd_voiture;

CREATE TABLE voiture(
	id_v INT PRIMARY KEY AUTO_INCREMENT,
    name_v VARCHAR(150) NOT NULL,
    year_v VARCHAR(4) NOT NULL,
    hp_v INT(3) NOT NULL,
    price_v double NOT NULL
);

CREATE TABLE person(
	id_p INT PRIMARY KEY AUTO_INCREMENT,
    firstname_p VARCHAR(50) NOT NULL,
    lastname_p VARCHAR(50) NOT NULL,
    age_p INT(3) NOT NULL
);

CREATE TABLE sale(
	id_v  INT UNIQUE NOT NULL REFERENCES  voiture (id_v),
    id_p INT NOT NULL REFERENCES person (id_p),
    date_s DATE,
    PRIMARY KEY(id_v, id_p)
);

SELECT * FROM voiture;
SELECT * FROM person;
SELECT * FROM sale;