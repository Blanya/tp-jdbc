package org.example.domain;

import java.io.Serializable;

public class Car implements Serializable {
    private int id;
    private String name;
    private String year;
    private int hp;
    private double price;

    public Car(String name, String year, int hp, double price) {
        this.name = name;
        this.year = year;
        this.hp = hp;
        this.price = price;
    }

    public Car(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Voiture{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", year='" + year + '\'' +
                ", hp=" + hp +
                ", price=" + price +
                '}';
    }
}
