package org.example.domain;

import org.example.dao.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.lang.Integer.parseInt;

public class Menu {

    static IDaoPerson iDaoPerson = new PersonDaoImpl();
    static IDaoCar iDaoCar = new CarDaoImpl();
    static IDaoSale iDaoSale = new SaleDaoImpl();
    public static void printMenu()
    {
        String[] menu = {"---MENU---", "1. Enregistrer la voiture", "2. Afficher la liste des voitures",
                "3. Supprimer une voiture", "4. Modifier une voiture", "5. Inscrire une personne",
                "6. Afficher la liste des personnes", "7. Supprimer une personne du listing", "8. Modifier une personne",
                "9. Faire la vente", "10. Afficher les ventes de voiture", "11. Afficher les ventes d'une personne",
                "12. Quitter"};

        for (String prop : menu) {
            System.out.println(prop);
        }

        Scanner sc = new Scanner(System.in);
        int choice = 0;

        try
        {
            choice = sc.nextInt();
        }catch(InputMismatchException e)
        {
            System.out.println("Erreur: Veuillez entrez un nombre");
            printMenu();
        }

        switch (choice) {
            case 1:
                System.out.println("Enregistrement d'une voiture");
                createCar();
                printMenu();
                break;
            case 2:
                System.out.println("Voici la liste des voitures");
                showAllCars();
                printMenu();
                break;
            case 3:
                System.out.println("Suppression d'une voiture");
                deleteCar();
                printMenu();
                break;
            case 4:
                System.out.println("Modification d'une voiture");
                updateCar();
                printMenu();
                break;
            case 5:
                System.out.println("Enregistrement d'une personne");
                createPerson();
                printMenu();
                break;
            case 6:
                System.out.println("Liste des personnes");
                showAllPersons();
                printMenu();
                break;
            case 7:
                System.out.println("Supprimer une personne du listing");
                deletePerson();
                printMenu();
                break;
            case 8:
                System.out.println("Modification d'une personne");
                updatePerson();
                printMenu();
                break;
            case 9:
                System.out.println("Effectuer une vente");
                createSale();
                printMenu();
                break;
            case 10:
                System.out.println("Afficher les ventes effectuées");
                showAllSales();
                printMenu();
                break;
            case 11:
                System.out.println("Les ventes selon une personne");
                getSalesByPerson();
                printMenu();
                break;
            case 12:
                System.out.println("Fermeture du menu !");
                break;
            default:
                System.out.println("Commande invalide, veuillez réessayer");
                printMenu();
        }
    }

    public static void createCar()
    {
        Scanner sc = new Scanner(System.in);

        try{
            System.out.println("Nom: ");
            String name = sc.nextLine();
            System.out.println("Année: ");
            String year = sc.next();
            System.out.println("Puissance chevaux: ");
            int hp = sc.nextInt();
            System.out.println("Prix: ");
            double price = sc.nextDouble();

            //get values from scanner
            Car c = new Car(name, year, hp, price);

            // result > 0 if executed in db
            Integer result = iDaoCar.createCar(c);

            if(result>0){
                System.out.println("Voiture ajoutée !");
            }else{
                System.out.println("Erreur dans la création de la voiture");
            }
        }catch (InputMismatchException ex){
            System.out.println("Données incorrectes, veuillez réessayer.");
            createCar();
        }
    }

    public static void createPerson()
    {
        Scanner sc = new Scanner(System.in);

        try{
            System.out.println("Nom: ");
            String lastname = sc.next();
            System.out.println("Prénom: ");
            String firstname = sc.next();
            System.out.println("Age: ");
            int age = sc.nextInt();

            //get values from scanner
            Person p = new Person(firstname, lastname, age);

            // result > 0 if executed in db
            Integer result = iDaoPerson.createPerson(p);

            if(result>0){
                System.out.println("Personne ajoutée !");
            }else{
                System.out.println("Erreur dans l'ajout de la personne'");
            }
        }catch (InputMismatchException ex){
            System.out.println("Données incorrectes, veuillez réessayer.");
            createPerson();
        }
    }

    public static void createSale()
    {
        Scanner sc = new Scanner(System.in);

        try{
            System.out.println("Id de la personne: ");
            int idP = sc.nextInt();
            System.out.println("Id de la voiture: ");
            int idV = sc.nextInt();
            System.out.println("Date (Format JJ/MM/AAAA: ");
            String date = sc.next();

            //recup date
            Date dateVente = null;
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            //check good format => 0<m<=12
            simpleDateFormat.setLenient(false);

            dateVente = simpleDateFormat.parse(date);


            //get values from scanner
            Person p = iDaoPerson.getPerson(idP);
            Car c = iDaoCar.getCar(idV);

            //if person or car null
            if(p == null || c == null)
            {
                if(p == null)
                {
                    System.out.println("La personne sélectionnée n'est pas enregistrée");
                }
                else
                    System.out.println("La voiture n'existe pas");
            }

            else
            {
                //get year to compare with car year
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(dateVente);
                int year = calendar.get(Calendar.YEAR);

                if(parseInt(c.getYear()) > year)
                {
                    System.out.println("L'année de vente n'est pas valide");
                }
                else {
                    Sale s = new Sale(p, c, dateVente);

                    // result > 0 if executed in db
                    Integer result = iDaoSale.createSale(s);

                    if(result>0){
                        System.out.println("Vente ajoutée !");
                    }else{
                        System.out.println("Erreur dans la création de la vente");
                    }
                }

            }

        }catch (InputMismatchException ex){
            System.out.println("Données incorrectes, veuillez réessayer.");
            createSale();
        } catch (ParseException e) {
            System.out.println("Format de la date non respectée : JJ/MM/AAAA");
            createSale();
        }
    }

    public static void showAllCars()
    {
        List<Car> cars = iDaoCar.getAllCars();

        for (Car c: cars ) {
            System.out.println(c);
        }
    }

    public static void showAllPersons()
    {
        List<Person> persons = iDaoPerson.getAllPersons();

        for (Person p: persons ) {
            System.out.println(p);
        }
    }

    public static void showAllSales()
    {
        List<Sale> sales = iDaoSale.getAllSales();

        for (Sale s: sales ) {
            System.out.println(s);
        }
    }


    public static void deleteCar()
    {
        System.out.println("Donnez-moi l'id de la voiture à supprimer");
        Scanner sc = new Scanner(System.in);

        try {
            int id = sc.nextInt();

            //check if product exists
            if(iDaoCar.getCar(id) != null)
            {
                int result = iDaoCar.deleteCar(id);

                if(result > 0)
                {
                    System.out.println("Voiture supprimée ! ");
                }
                else
                {
                    System.out.println("Erreur lors de la suppression");
                }
            }
            //product doesn't exist
            else
            {
                System.out.println("La voiture n'existe pas, veuillez réessayer");
                deleteCar();
            }

        } catch (InputMismatchException e)
        {
            System.out.println("Veuillez entrer un nombre");
        }
    }

    public static void deletePerson()
    {
        System.out.println("Donnez-moi l'id de la personne à supprimer du listing");
        Scanner sc = new Scanner(System.in);

        try {
            int id = sc.nextInt();

            //check if product exists
            if(iDaoPerson.getPerson(id) != null)
            {
                int result = iDaoPerson.deletePerson(id);

                if(result > 0)
                {
                    System.out.println("Personne supprimée du listing ! ");
                }
                else
                {
                    System.out.println("Erreur lors de la suppression");
                }
            }
            //product doesn't exist
            else
            {
                System.out.println("La personne n'existe pas dans nos fichiers, veuillez réessayer");
                deletePerson();
            }

        } catch (InputMismatchException e)
        {
            System.out.println("Veuillez entrer un nombre");
        }
    }

    public static void updateCar()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Donnez-moi le numéro de la voiture à modifier et les élèments à modifier");
        System.out.println("Id de la voiture : ");

        try{
            int id = sc.nextInt();
            Car c = iDaoCar.getCar(id);

            // p == null if id doesn't exist product same
            if(c == null)
            {
                System.out.println("La voiture n'existe pas, veuillez entrer un numéro de voiture valide");
                updateCar();
            }
            else
            {
                //get product values and new values
                System.out.printf("Nom[%s] :", c.getName());
                sc.nextLine();
                String name = sc.nextLine();

                System.out.printf("Année[%s] :", c.getYear());
                String year = sc.next();

                System.out.printf("Puissance chevaux[%d] :", c.getHp());
                int hp = sc.nextInt();

                System.out.printf("Prix[%f] :", c.getPrice());
                double price = sc.nextDouble();

                c.setName(name);
                c.setYear(year);
                c.setHp(hp);
                c.setPrice(price);

                int result = iDaoCar.updateCar(c);

                if(result > 0)
                {
                    System.out.println("La voiture a bien été mise à jour");
                }
                else
                {
                    System.out.println("Erreur lors de la modification de la voiture");
                }
            }
        }catch (InputMismatchException e)
        {
            System.out.println("Les données insérées sont incorrectes, veuillez réessayer");
            updateCar();
        }
    }

    public static void updatePerson()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Donnez-moi le numéro de la personne à modifier et les élèments à modifier");
        System.out.println("Numéro de la personne : ");

        try{
            int id = sc.nextInt();
            Person p = iDaoPerson.getPerson(id);

            // p == null if id doesn't exist product same
            if(p == null)
            {
                System.out.println("La personne n'existe pas dans nos fichiers, veuillez entrer un numéro de personne valide");
                updatePerson();
            }
            else
            {
                //get product values and new values
                System.out.printf("Nom[%s] :", p.getFirstName());
                sc.nextLine();
                String firstname = sc.next();

                System.out.printf("Prénom[%s] :", p.getLastName());
                String lastname = sc.next();

                System.out.printf("Age[%d] :", p.getAge());
                int age = sc.nextInt();

                p.setFirstName(firstname);
                p.setLastName(lastname);
                p.setAge(age);

                int result = iDaoPerson.updatePerson(p);

                if(result > 0)
                {
                    System.out.println("Les infos de la personne sont désormais à jour");
                }
                else
                {
                    System.out.println("Erreur lors de la modification des infos de la personne");
                }
            }
        }catch (InputMismatchException e)
        {
            System.out.println("Les données insérées sont incorrectes, veuillez réessayer");
            updatePerson();
        }
    }

    public static void getSalesByPerson()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Quel est le numéro de la personne ?");

        try{
            int id = sc.nextInt();
            Person p = iDaoPerson.getPerson(id);

            //if id doesn't exist => person == null
            if(p == null)
            {
                System.out.println("La personne recherchée n'existe pas dans nos fichiers");
            }
            else
            {
                List<Sale> sales = iDaoSale.getSalesByPerson(id);
                for (Sale s: sales ) {
                    System.out.println(s);
                }
            }
        }catch (InputMismatchException e)
        {
            System.out.println("Erreur: Veuillez entrez un nombre");
            getSalesByPerson();
        }
    }
}
