package org.example.domain;

import java.io.Serializable;
import java.util.Date;

public class Sale implements Serializable {
    Person person;
    Car car;
    private Date date;

    public Sale(Person person, Car car, Date date) {
        this.person = person;
        this.car = car;
        this.date = date;
    }

    public Sale(){

    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Vente{" +
                "person=" + person +
                ", car=" + car +
                ", date=" + date +
                '}';
    }
}
