package org.example.dao;

import org.example.domain.Car;
import org.example.utils.ConnectionUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CarDaoImpl implements IDaoCar{

    @Override
    public Car getCar(Integer id) {
        Car c = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;

        try {
            connection = ConnectionUtils.getMySQLConnection();
            String sql = "SELECT * FROM voiture WHERE id_v = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            rs = statement.executeQuery();

            if (rs.next()){
                c = new Car(rs.getString("name_v"),
                        rs.getString("year_v"),
                        rs.getInt("hp_v"),
                        rs.getDouble("price_v"));

                c.setId(rs.getInt("id_v"));
            }
        }catch (SQLSyntaxErrorException ex) {
            System.err.println("Error : " + ex.getMessage());
        }
        catch (SQLException exception1)
        {
            exception1.printStackTrace();
        }
        finally {
            try {
                if(rs != null)
                    rs.close();
                if(statement != null)
                    statement.close();
                if(connection != null)
                    connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        return c;
    }

    @Override
    public List<Car> getAllCars() {
        List<Car> cars = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;

        try {
            connection = ConnectionUtils.getMySQLConnection();
            String sql = "SELECT * FROM voiture";
            statement = connection.prepareStatement(sql);
            rs = statement.executeQuery();

            while (rs.next()){
                Car c = new Car();
                c.setId(rs.getInt("id_v"));
                c.setName(rs.getString("name_v"));
                c.setYear(rs.getString("year_v"));
                c.setHp(rs.getInt("hp_v"));
                c.setPrice(rs.getDouble("price_v"));

                cars.add(c);
            }
        }catch (SQLSyntaxErrorException ex) {
            System.err.println("Error : " + ex.getMessage());
        }
        catch (SQLException exception1)
        {
            exception1.printStackTrace();
        }
        finally {
            try {
                if(rs != null)
                    rs.close();
                if(statement != null)
                    statement.close();
                if(connection != null)
                    connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        return cars;
    }

    @Override
    public Integer createCar(Car car) {
        Connection connection = null;
        PreparedStatement statement = null;
        int result = 0;

        try {
            connection = ConnectionUtils.getMySQLConnection();
            connection.setAutoCommit(false);
            String sql = "INSERT INTO voiture(name_v, year_v, hp_v, price_v) VALUES (?, ?, ?, ?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1, car.getName());
            statement.setString(2, car.getYear());
            statement.setInt(3, car.getHp());
            statement.setDouble(4, car.getPrice());

            result = statement.executeUpdate();

            connection.commit();

        }catch (SQLSyntaxErrorException ex) {
            System.err.println("Error : " + ex.getMessage());
        } catch (SQLException exception1)
        {
            System.out.println(exception1.getMessage());
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        finally {
            try {
                if(statement != null)
                    statement.close();
                if(connection != null)
                    connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        return result;
    }

    @Override
    public Integer updateCar(Car car) {
        Connection connection = null;
        PreparedStatement statement = null;
        int result = 0;

        try {
            connection = ConnectionUtils.getMySQLConnection();
            connection.setAutoCommit(false);

            String sql = "UPDATE voiture SET name_v = ?, year_v = ? , hp_v = ?, price_v = ? WHERE id_v = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, car.getName());
            statement.setString(2, car.getYear());
            statement.setInt(3, car.getHp());
            statement.setDouble(4, car.getPrice());
            statement.setInt(5, car.getId());

            result = statement.executeUpdate();

            connection.commit();

        }catch (SQLSyntaxErrorException ex) {
            System.err.println("Error : " + ex.getMessage());
        } catch (SQLException exception1)
        {
            System.out.println(exception1.getMessage());
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        finally {
            try {
                if(statement != null)
                    statement.close();
                if(connection != null)
                    connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        return result;
    }

    @Override
    public Integer deleteCar(Integer id) {
        Connection connection = null;
        PreparedStatement statement = null;
        PreparedStatement statement1 = null;
        int result = 0;
        int result1 = 0;

        try {
            connection = ConnectionUtils.getMySQLConnection();
            connection.setAutoCommit(false);

            String sql = "DELETE FROM voiture WHERE id_v = ?";

            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);

            result = statement.executeUpdate();

            String sql1 = "DELETE FROM sale WHERE id_v = ?";

            statement1 = connection.prepareStatement(sql1);
            statement1.setInt(1, id);

            result1 = statement1.executeUpdate();

            connection.commit();

        }catch (SQLSyntaxErrorException ex) {
            System.err.println("Error : " + ex.getMessage());
        } catch (SQLException exception1)
        {
            System.out.println(exception1.getMessage());
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        finally {
            try {
                if(statement != null)
                    statement.close();
                if(connection != null)
                    connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        return result + result1;
    }
}
