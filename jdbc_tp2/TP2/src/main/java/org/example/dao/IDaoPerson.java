package org.example.dao;

import org.example.domain.Person;

import java.util.List;

public interface IDaoPerson {
    Person getPerson(Integer id);

    List<Person> getAllPersons();

    Integer createPerson(Person person);

    Integer updatePerson(Person person);

    Integer deletePerson(Integer id);
}
