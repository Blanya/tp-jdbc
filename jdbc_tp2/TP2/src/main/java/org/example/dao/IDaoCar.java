package org.example.dao;

import org.example.domain.Car;

import java.util.List;

public interface IDaoCar {
    Car getCar(Integer id);

    List<Car> getAllCars();

    Integer createCar(Car car);

    Integer updateCar(Car car);

    Integer deleteCar(Integer id);
}
