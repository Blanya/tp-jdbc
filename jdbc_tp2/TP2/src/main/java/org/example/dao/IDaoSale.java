package org.example.dao;

import org.example.domain.Sale;

import java.util.List;

public interface IDaoSale {
    List<Sale> getAllSales();

    List<Sale> getSalesByPerson(Integer id_p);

    int createSale(Sale sale);
}
