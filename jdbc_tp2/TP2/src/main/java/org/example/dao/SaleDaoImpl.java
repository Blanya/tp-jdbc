package org.example.dao;

import org.example.domain.Car;
import org.example.domain.Person;
import org.example.domain.Sale;
import org.example.utils.ConnectionUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SaleDaoImpl implements IDaoSale{

    //use idaocar and idaoperson to get car and person
    IDaoPerson iDaoPerson = new PersonDaoImpl();
    IDaoCar iDaoCar = new CarDaoImpl();


    @Override
    public List<Sale> getAllSales() {
        List<Sale> sales = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;

        try {
            connection = ConnectionUtils.getMySQLConnection();
            String sql = "SELECT * FROM sale";
            statement = connection.prepareStatement(sql);
            rs = statement.executeQuery();

            while (rs.next()){
                Sale s = new Sale();
                Person p = new Person();
                Car c = new Car();

                p = iDaoPerson.getPerson(rs.getInt("id_p"));
                c = iDaoCar.getCar(rs.getInt("id_v"));

                s.setCar(c);
                s.setPerson(p);
                s.setDate(rs.getDate("date_s"));

                sales.add(s);
            }
        }catch (SQLSyntaxErrorException ex) {
            System.err.println("Error : " + ex.getMessage());
        }
        catch (SQLException exception1)
        {
            exception1.printStackTrace();
        }
        finally {
            try {
                if(rs != null)
                    rs.close();
                if(statement != null)
                    statement.close();
                if(connection != null)
                    connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        return sales;
    }

    @Override
    public List<Sale> getSalesByPerson(Integer id_p) {
        List<Sale> sales = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;

        try {
            connection = ConnectionUtils.getMySQLConnection();
            String sql = "SELECT * FROM sale WHERE id_p = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id_p);
            rs = statement.executeQuery();

            while (rs.next()){
                Sale s = new Sale();
                Car c = new Car();

                Person p = iDaoPerson.getPerson(id_p);
                c = iDaoCar.getCar(rs.getInt("id_v"));

                s.setPerson(p);
                s.setCar(c);
                s.setDate(rs.getDate("date_s"));

                sales.add(s);
            }
        }catch (SQLSyntaxErrorException ex) {
            System.err.println("Error : " + ex.getMessage());
        }
        catch (SQLException exception1)
        {
            exception1.printStackTrace();
        }
        finally {
            try {
                if(rs != null)
                    rs.close();
                if(statement != null)
                    statement.close();
                if(connection != null)
                    connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        return sales;
    }

    @Override
    public int createSale(Sale sale) {
        Connection connection = null;
        PreparedStatement statement = null;
        int result = 0;

        try {
            connection = ConnectionUtils.getMySQLConnection();
            connection.setAutoCommit(false);

            String sql = "INSERT INTO sale(id_v, id_p, date_s) VALUES (?, ?, ?)";
            statement = connection.prepareStatement(sql);

            statement.setInt(1, sale.getCar().getId());
            statement.setInt(2, sale.getPerson().getId());
            statement.setDate(3, new Date(sale.getDate().getTime()));

            result = statement.executeUpdate();

            connection.commit();

        }catch (SQLSyntaxErrorException ex) {
            System.err.println("Error : " + ex.getMessage());
        } catch (SQLException exception1)
        {
            System.out.println(exception1.getMessage());
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        finally {
            try {
                if(statement != null)
                    statement.close();
                if(connection != null)
                    connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        return result;
    }
}
