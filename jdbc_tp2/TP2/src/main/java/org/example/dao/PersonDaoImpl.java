package org.example.dao;

import org.example.domain.Car;
import org.example.domain.Person;
import org.example.utils.ConnectionUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PersonDaoImpl implements IDaoPerson {

    @Override
    public Person getPerson(Integer id) {
        Person p = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;

        try {
            connection = ConnectionUtils.getMySQLConnection();
            String sql = "SELECT * FROM person WHERE id_p = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);
            rs = statement.executeQuery();

            if (rs.next()){
                p = new Person(rs.getString("firstname_p"),
                        rs.getString("lastname_p"),
                        rs.getInt("age_p"));

                p.setId(rs.getInt("id_p"));
            }
        }catch (SQLSyntaxErrorException ex) {
            System.err.println("Error : " + ex.getMessage());
        }
        catch (SQLException exception1)
        {
            exception1.printStackTrace();
        }
        finally {
            try {
                if(rs != null)
                    rs.close();
                if(statement != null)
                    statement.close();
                if(connection != null)
                    connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        return p;
    }

    @Override
    public List<Person> getAllPersons() {
        List<Person> persons = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;

        try {
            connection = ConnectionUtils.getMySQLConnection();
            String sql = "SELECT * FROM person";
            statement = connection.prepareStatement(sql);
            rs = statement.executeQuery();

            while (rs.next()){
                Person p = new Person();
                p.setId(rs.getInt("id_p"));
                p.setFirstName(rs.getString("firstname_p"));
                p.setLastName(rs.getString("lastname_p"));
                p.setAge(rs.getInt("age_p"));

                persons.add(p);
            }
        }catch (SQLSyntaxErrorException ex) {
            System.err.println("Error : " + ex.getMessage());
        }
        catch (SQLException exception1)
        {
            exception1.printStackTrace();
        }
        finally {
            try {
                if(rs != null)
                    rs.close();
                if(statement != null)
                    statement.close();
                if(connection != null)
                    connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        return persons;
    }

    @Override
    public Integer createPerson(Person person) {
        Connection connection = null;
        PreparedStatement statement = null;
        int result = 0;

        try {
            connection = ConnectionUtils.getMySQLConnection();
            connection.setAutoCommit(false);
            String sql = "INSERT INTO person(firstname_p, lastname_p, age_p) VALUES (?, ?, ?)";
            statement = connection.prepareStatement(sql);
            statement.setString(1, person.getFirstName());
            statement.setString(2, person.getLastName());
            statement.setInt(3, person.getAge());

            result = statement.executeUpdate();

            connection.commit();

        }catch (SQLSyntaxErrorException ex) {
            System.err.println("Error : " + ex.getMessage());
        } catch (SQLException exception1)
        {
            System.out.println(exception1.getMessage());
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        finally {
            try {
                if(statement != null)
                    statement.close();
                if(connection != null)
                    connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        return result;
    }

    @Override
    public Integer updatePerson(Person person) {
        Connection connection = null;
        PreparedStatement statement = null;
        int result = 0;

        try {
            connection = ConnectionUtils.getMySQLConnection();
            connection.setAutoCommit(false);
            String sql = "UPDATE person SET firstname_p = ?, lastname_p = ?, age_p = ? WHERE id_p = ?";
            statement = connection.prepareStatement(sql);
            statement.setString(1, person.getFirstName());
            statement.setString(2, person.getLastName());
            statement.setInt(3, person.getAge());
            statement.setInt(4, person.getId());

            result = statement.executeUpdate();

            connection.commit();

        }catch (SQLSyntaxErrorException ex) {
            System.err.println("Error : " + ex.getMessage());
        } catch (SQLException exception1)
        {
            System.out.println(exception1.getMessage());
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        finally {
            try {
                if(statement != null)
                    statement.close();
                if(connection != null)
                    connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        return result;
    }

    @Override
    public Integer deletePerson(Integer id) {
        Connection connection = null;
        PreparedStatement statement = null;
        PreparedStatement statement1 = null;
        int result = 0;
        int result1 = 0;

        try {
            connection = ConnectionUtils.getMySQLConnection();
            connection.setAutoCommit(false);
            String sql = "DELETE FROM person WHERE id_p = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1, id);

            result = statement.executeUpdate();

            String sql1 = "DELETE FROM sale WHERE id_p = ?";
            statement1 = connection.prepareStatement(sql1);
            statement1.setInt(1, id);

            result1 = statement1.executeUpdate();

            connection.commit();

        }catch (SQLSyntaxErrorException ex) {
            System.err.println("Error : " + ex.getMessage());
        } catch (SQLException exception1)
        {
            System.out.println(exception1.getMessage());
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        finally {
            try {
                if(statement != null)
                    statement.close();
                if(connection != null)
                    connection.close();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
        result += result1;
        return result;
    }
}
